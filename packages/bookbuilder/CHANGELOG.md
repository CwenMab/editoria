# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.3.0"></a>
# [1.3.0](https://gitlab.coko.foundation/editoria/editoria/compare/pubsweet-component-bookbuilder@1.2.6...pubsweet-component-bookbuilder@1.3.0) (2019-08-01)


### Bug Fixes

* **applicationmanager:** add data-test-id for testing purposes ([5562b9a](https://gitlab.coko.foundation/editoria/editoria/commit/5562b9a))
* **applicationmanager:** broke state of bookcomponent ([3cc4f83](https://gitlab.coko.foundation/editoria/editoria/commit/3cc4f83))
* **applicationmanager:** fix models retrieve confgi form db ([41f9a92](https://gitlab.coko.foundation/editoria/editoria/commit/41f9a92))
* **applicationmanager:** fix models retrieve confgi form db ([e441cb1](https://gitlab.coko.foundation/editoria/editoria/commit/e441cb1))
* **applicationmanager:** set default componentType for missing wax config ([8f9932c](https://gitlab.coko.foundation/editoria/editoria/commit/8f9932c))
* **applicationparameters:** rename config to applicationmanager props everywhere ([47b3538](https://gitlab.coko.foundation/editoria/editoria/commit/47b3538))
* **dynamiccomponenttypes:** add data-test-id for tests ([fd52e45](https://gitlab.coko.foundation/editoria/editoria/commit/fd52e45))
* **test:** make test work for editoria ([8868af9](https://gitlab.coko.foundation/editoria/editoria/commit/8868af9))
* **waxPubsweet:** fix subscritpions ([ef11796](https://gitlab.coko.foundation/editoria/editoria/commit/ef11796))


### Features

* **applicationmanager:** get live updates subscriptions ([632e471](https://gitlab.coko.foundation/editoria/editoria/commit/632e471))
* **applicationparameters:** get config from db create graphql query ([3a34792](https://gitlab.coko.foundation/editoria/editoria/commit/3a34792))
* **header:** easier navigation between bookcomponents ([9e675af](https://gitlab.coko.foundation/editoria/editoria/commit/9e675af))




<a name="1.2.6"></a>
## [1.2.6](https://gitlab.coko.foundation/editoria/editoria/compare/pubsweet-component-bookbuilder@1.2.5...pubsweet-component-bookbuilder@1.2.6) (2019-05-28)




**Note:** Version bump only for package pubsweet-component-bookbuilder

<a name="1.2.5"></a>
## [1.2.5](https://gitlab.coko.foundation/editoria/editoria/compare/pubsweet-component-bookbuilder@1.2.4...pubsweet-component-bookbuilder@1.2.5) (2019-05-17)


### Bug Fixes

* restriction for file extension for Linux, handling of ink error ([4fb0cc5](https://gitlab.coko.foundation/editoria/editoria/commit/4fb0cc5))




<a name="1.2.4"></a>
## [1.2.4](https://gitlab.coko.foundation/editoria/editoria/compare/pubsweet-component-bookbuilder@1.2.3...pubsweet-component-bookbuilder@1.2.4) (2019-04-25)


### Bug Fixes

* publication date fix ([88cbfea](https://gitlab.coko.foundation/editoria/editoria/commit/88cbfea))




<a name="1.2.3"></a>
## [1.2.3](https://gitlab.coko.foundation/editoria/editoria/compare/pubsweet-component-bookbuilder@1.2.2...pubsweet-component-bookbuilder@1.2.3) (2019-04-25)


### Bug Fixes

* more fixes ([f6028f5](https://gitlab.coko.foundation/editoria/editoria/commit/f6028f5))




<a name="1.2.2"></a>
## [1.2.2](https://gitlab.coko.foundation/editoria/editoria/compare/pubsweet-component-bookbuilder@1.2.1...pubsweet-component-bookbuilder@1.2.2) (2019-04-24)


### Bug Fixes

* **authorize:**  can fragment edit rule  foreach bookcomponent ([7a6c4c0](https://gitlab.coko.foundation/editoria/editoria/commit/7a6c4c0))
* **authsome:** canViewFragment undefined ([60ca32b](https://gitlab.coko.foundation/editoria/editoria/commit/60ca32b))
* **style:** teammanager  style ([0fa7d10](https://gitlab.coko.foundation/editoria/editoria/commit/0fa7d10))
* more UI fixes ([78e14da](https://gitlab.coko.foundation/editoria/editoria/commit/78e14da))
* UI fixes ([92841d3](https://gitlab.coko.foundation/editoria/editoria/commit/92841d3))




<a name="1.2.1"></a>
## [1.2.1](https://gitlab.coko.foundation/editoria/editoria/compare/pubsweet-component-bookbuilder@1.2.0...pubsweet-component-bookbuilder@1.2.1) (2019-04-15)


### Bug Fixes

* wax export fix, invalid url fixes ([c42a96f](https://gitlab.coko.foundation/editoria/editoria/commit/c42a96f))




<a name="1.2.0"></a>
# [1.2.0](https://gitlab.coko.foundation/editoria/editoria/compare/pubsweet-component-bookbuilder@1.1.5...pubsweet-component-bookbuilder@1.2.0) (2019-04-12)


### Bug Fixes

* **authorize:** find bookcomponent State for the correct workflowState ([752bc7c](https://gitlab.coko.foundation/editoria/editoria/commit/752bc7c))
* **authsome:** fix subscription for authosme updates ([1ce1fd9](https://gitlab.coko.foundation/editoria/editoria/commit/1ce1fd9))
* **authsome:** move team rules to bookbuilder ([0cab675](https://gitlab.coko.foundation/editoria/editoria/commit/0cab675))
* **bookcomponent:** add view - edit to bookcomponent button ([b2738e5](https://gitlab.coko.foundation/editoria/editoria/commit/b2738e5))
* **editingNotification:** pass currentUser to access admin ([78b4aa9](https://gitlab.coko.foundation/editoria/editoria/commit/78b4aa9))
* **teammanager:** teamanger add bug as search ([631c579](https://gitlab.coko.foundation/editoria/editoria/commit/631c579))
* **upgrade:** remove redux dependencies upgrade client ([be4d87b](https://gitlab.coko.foundation/editoria/editoria/commit/be4d87b))


### Features

* closes [#172](https://gitlab.coko.foundation/editoria/editoria/issues/172), [#189](https://gitlab.coko.foundation/editoria/editoria/issues/189) and [#171](https://gitlab.coko.foundation/editoria/editoria/issues/171) ([3deb559](https://gitlab.coko.foundation/editoria/editoria/commit/3deb559))
* **authorize:** update rules in certain events fo the user ([89ee01e](https://gitlab.coko.foundation/editoria/editoria/commit/89ee01e))
* **authsome:** add subscriptions to get updates ([c042093](https://gitlab.coko.foundation/editoria/editoria/commit/c042093))
* **authsome:** move authorize to backend ([4e00def](https://gitlab.coko.foundation/editoria/editoria/commit/4e00def))
* **bookbuilder:** drag and drop ([bd21f36](https://gitlab.coko.foundation/editoria/editoria/commit/bd21f36))
* **dashboard:** archive books and user's full name support ([8bd0a25](https://gitlab.coko.foundation/editoria/editoria/commit/8bd0a25)), closes [#226](https://gitlab.coko.foundation/editoria/editoria/issues/226) [#197](https://gitlab.coko.foundation/editoria/editoria/issues/197)
* **dashboard:** prepare ui for sorting ([80405b2](https://gitlab.coko.foundation/editoria/editoria/commit/80405b2))




<a name="1.1.5"></a>
## [1.1.5](https://gitlab.coko.foundation/editoria/editoria/compare/pubsweet-component-bookbuilder@1.1.4...pubsweet-component-bookbuilder@1.1.5) (2018-11-20)




**Note:** Version bump only for package pubsweet-component-bookbuilder
