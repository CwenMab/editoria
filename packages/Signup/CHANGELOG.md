# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.1.3"></a>
## [1.1.3](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/editoria-component-signup@1.1.2...editoria-component-signup@1.1.3) (2019-05-28)




**Note:** Version bump only for package editoria-component-signup

<a name="1.1.2"></a>
## [1.1.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/editoria-component-signup@1.1.1...editoria-component-signup@1.1.2) (2019-05-22)


### Bug Fixes

* **signup:** fix permission rule when new user created ([bea7622](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/bea7622))




<a name="1.1.1"></a>
## [1.1.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/editoria-component-signup@1.1.0...editoria-component-signup@1.1.1) (2019-05-17)


### Bug Fixes

* **signup:** required surname and given name, redirect to login ([d7ffeb0](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/d7ffeb0))




<a name="1.1.0"></a>
# 1.1.0 (2019-04-12)


### Bug Fixes

* **upgrade:** remove redux dependencies upgrade client ([be4d87b](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/be4d87b))


### Features

* **app:** Given name and surname now supported in Signup ([745ab2e](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/745ab2e)), closes [#197](https://gitlab.coko.foundation/pubsweet/pubsweet/issues/197)
* **components:** add to monorepo signup and login ([a8500e3](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/a8500e3))
* **components:** add to monorepo signup and login ([2b6a1f2](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/2b6a1f2))
* **components:** add to monorepo signup and login ([420c1c3](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/420c1c3))
* **dashboard:** full name for user used in sorting ([64ec470](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/64ec470))
