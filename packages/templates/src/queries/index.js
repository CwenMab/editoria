export { default as createTemplateMutation } from './createTemplate'
export { default as deleteTemplateMutation } from './deleteTemplate'
export { default as getTemplatesQuery } from './getTemplates'
export { default as getTemplateQuery } from './getTemplate'
export { default as updateTemplateMutation } from './updateTemplate'
export {
  templateCreatedSubscription,
  templateUpdatedSubscription,
  templateDeletedSubscription,
} from './templateSubscriptions'
