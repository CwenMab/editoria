# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="0.1.1"></a>
## 0.1.1 (2019-08-01)


### Bug Fixes

* **app:** fixes for the beta release ([2ee72a4](https://gitlab.coko.foundation/editoria/editoria-templates/commit/2ee72a4))
