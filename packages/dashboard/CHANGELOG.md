# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="0.3.0"></a>
# [0.3.0](https://gitlab.coko.foundation/editoria/editoria-dashboard/compare/pubsweet-component-editoria-dashboard@0.2.6...pubsweet-component-editoria-dashboard@0.3.0) (2019-08-01)


### Bug Fixes

* **test:** make test work for editoria ([8868af9](https://gitlab.coko.foundation/editoria/editoria-dashboard/commit/8868af9))


### Features

* **applicationparameters:** get config from db create graphql query ([3a34792](https://gitlab.coko.foundation/editoria/editoria-dashboard/commit/3a34792))




<a name="0.2.6"></a>
## [0.2.6](https://gitlab.coko.foundation/editoria/editoria-dashboard/compare/pubsweet-component-editoria-dashboard@0.2.5...pubsweet-component-editoria-dashboard@0.2.6) (2019-05-28)




**Note:** Version bump only for package pubsweet-component-editoria-dashboard

<a name="0.2.5"></a>
## [0.2.5](https://gitlab.coko.foundation/editoria/editoria-dashboard/compare/pubsweet-component-editoria-dashboard@0.2.4...pubsweet-component-editoria-dashboard@0.2.5) (2019-05-28)




**Note:** Version bump only for package pubsweet-component-editoria-dashboard

<a name="0.2.4"></a>
## [0.2.4](https://gitlab.coko.foundation/editoria/editoria-dashboard/compare/pubsweet-component-editoria-dashboard@0.2.3...pubsweet-component-editoria-dashboard@0.2.4) (2019-05-23)




**Note:** Version bump only for package pubsweet-component-editoria-dashboard

<a name="0.2.3"></a>
## [0.2.3](https://gitlab.coko.foundation/editoria/editoria-dashboard/compare/pubsweet-component-editoria-dashboard@0.2.2...pubsweet-component-editoria-dashboard@0.2.3) (2019-05-17)




**Note:** Version bump only for package pubsweet-component-editoria-dashboard

<a name="0.2.2"></a>
## [0.2.2](https://gitlab.coko.foundation/editoria/editoria-dashboard/compare/pubsweet-component-editoria-dashboard@0.2.1...pubsweet-component-editoria-dashboard@0.2.2) (2019-04-25)


### Bug Fixes

* more fixes ([f6028f5](https://gitlab.coko.foundation/editoria/editoria-dashboard/commit/f6028f5))




<a name="0.2.1"></a>
## [0.2.1](https://gitlab.coko.foundation/editoria/editoria-dashboard/compare/pubsweet-component-editoria-dashboard@0.2.0...pubsweet-component-editoria-dashboard@0.2.1) (2019-04-24)


### Bug Fixes

* UI fixes ([92841d3](https://gitlab.coko.foundation/editoria/editoria-dashboard/commit/92841d3))




<a name="0.2.0"></a>
# [0.2.0](https://gitlab.coko.foundation/editoria/editoria-dashboard/compare/pubsweet-component-editoria-dashboard@0.1.4...pubsweet-component-editoria-dashboard@0.2.0) (2019-04-12)


### Bug Fixes

* **authsome:** duplicate prop remove ([10f206d](https://gitlab.coko.foundation/editoria/editoria-dashboard/commit/10f206d))
* **authsome:** fix subscription for authosme updates ([1ce1fd9](https://gitlab.coko.foundation/editoria/editoria-dashboard/commit/1ce1fd9))
* **authsome:** move team rules to bookbuilder ([0cab675](https://gitlab.coko.foundation/editoria/editoria-dashboard/commit/0cab675))
* **authsome:** remove authorize component ([b3924bd](https://gitlab.coko.foundation/editoria/editoria-dashboard/commit/b3924bd))
* **dashboard:** correct named fn in create book mutation ([cd6a94a](https://gitlab.coko.foundation/editoria/editoria-dashboard/commit/cd6a94a))
* **dashboard:** ui fix for action underlines being inconsistent ([f8da624](https://gitlab.coko.foundation/editoria/editoria-dashboard/commit/f8da624))
* **teammanager:** teamanger add bug as search ([631c579](https://gitlab.coko.foundation/editoria/editoria-dashboard/commit/631c579))
* **upgrade:** remove redux dependencies upgrade client ([be4d87b](https://gitlab.coko.foundation/editoria/editoria-dashboard/commit/be4d87b))


### Features

* **authorize:** update rules in certain events fo the user ([89ee01e](https://gitlab.coko.foundation/editoria/editoria-dashboard/commit/89ee01e))
* **authsome:** add subscriptions to get updates ([c042093](https://gitlab.coko.foundation/editoria/editoria-dashboard/commit/c042093))
* **authsome:** move authorize to backend ([4e00def](https://gitlab.coko.foundation/editoria/editoria-dashboard/commit/4e00def))
* **dashboard:** archive books and user's full name support ([8bd0a25](https://gitlab.coko.foundation/editoria/editoria-dashboard/commit/8bd0a25)), closes [#226](https://gitlab.coko.foundation/editoria/editoria-dashboard/issues/226) [#197](https://gitlab.coko.foundation/editoria/editoria-dashboard/issues/197)
* **dashboard:** build sorting ui and add state handlers ([0cfe191](https://gitlab.coko.foundation/editoria/editoria-dashboard/commit/0cfe191))
* **dashboard:** full name for user used in sorting ([64ec470](https://gitlab.coko.foundation/editoria/editoria-dashboard/commit/64ec470))
* **dashboard:** make action separators gray and fatter ([00d0843](https://gitlab.coko.foundation/editoria/editoria-dashboard/commit/00d0843))
* **dashboard:** make icon ui for sorting ascending / descending order ([44417ee](https://gitlab.coko.foundation/editoria/editoria-dashboard/commit/44417ee))
* **dashboard:** make ui for book author & published status ([bf3f126](https://gitlab.coko.foundation/editoria/editoria-dashboard/commit/bf3f126))
* **dashboard:** prepare ui for sorting ([80405b2](https://gitlab.coko.foundation/editoria/editoria-dashboard/commit/80405b2))
* **dashboard:** sorting books in dashboard ([d22c27e](https://gitlab.coko.foundation/editoria/editoria-dashboard/commit/d22c27e))
* **dashboard:** wire up refetch ui with graphql ([aa99cef](https://gitlab.coko.foundation/editoria/editoria-dashboard/commit/aa99cef))




<a name="0.1.4"></a>
## [0.1.4](https://gitlab.coko.foundation/editoria/editoria-dashboard/compare/pubsweet-component-editoria-dashboard@0.1.3...pubsweet-component-editoria-dashboard@0.1.4) (2018-11-26)


### Bug Fixes

* use of the correct function's name ([82d2701](https://gitlab.coko.foundation/editoria/editoria-dashboard/commit/82d2701))




<a name="0.1.3"></a>
## [0.1.3](https://gitlab.coko.foundation/editoria/editoria-dashboard/compare/pubsweet-component-editoria-dashboard@0.1.2...pubsweet-component-editoria-dashboard@0.1.3) (2018-11-20)




**Note:** Version bump only for package pubsweet-component-editoria-dashboard
