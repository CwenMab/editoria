# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="0.1.7"></a>
## [0.1.7](https://gitlab.coko.foundation/editoria/editoria/compare/editoria-common@0.1.6...editoria-common@0.1.7) (2019-08-01)


### Bug Fixes

* **test:** make test work for editoria ([8868af9](https://gitlab.coko.foundation/editoria/editoria/commit/8868af9))




<a name="0.1.6"></a>
## [0.1.6](https://gitlab.coko.foundation/editoria/editoria/compare/editoria-common@0.1.5...editoria-common@0.1.6) (2019-05-28)




**Note:** Version bump only for package editoria-common

<a name="0.1.5"></a>
## [0.1.5](https://gitlab.coko.foundation/editoria/editoria/compare/editoria-common@0.1.4...editoria-common@0.1.5) (2019-05-17)


### Bug Fixes

* restriction for file extension for Linux, handling of ink error ([4fb0cc5](https://gitlab.coko.foundation/editoria/editoria/commit/4fb0cc5))




<a name="0.1.4"></a>
## [0.1.4](https://gitlab.coko.foundation/editoria/editoria/compare/editoria-common@0.1.3...editoria-common@0.1.4) (2019-04-24)


### Bug Fixes

* UI fixes ([92841d3](https://gitlab.coko.foundation/editoria/editoria/commit/92841d3))




<a name="0.1.3"></a>
## [0.1.3](https://gitlab.coko.foundation/editoria/editoria/compare/editoria-common@0.1.2...editoria-common@0.1.3) (2019-04-12)




**Note:** Version bump only for package editoria-common

<a name="0.1.2"></a>
## [0.1.2](https://gitlab.coko.foundation/editoria/editoria/compare/editoria-common@0.1.1...editoria-common@0.1.2) (2018-11-20)




**Note:** Version bump only for package editoria-common
