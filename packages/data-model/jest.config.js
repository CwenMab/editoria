module.exports = {
  testEnvironment: 'jest-environment-db',
  testPathIgnorePatterns: [
    './config',
    './__tests__/helpers',
    './node_modules/',
  ],
}
